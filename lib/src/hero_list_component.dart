import 'package:angular/angular.dart';
import 'hero_service.dart';
import 'package:tour_of_heros/src/hero.dart';
import 'mock_heros.dart';
import 'hero_component.dart';

@Component(
  selector: 'my-heros',
  directives: [coreDirectives, HeroComponent],
  templateUrl: 'hero_list_component.html',
  styleUrls: ['hero_list_component.css'],
  providers: [ClassProvider(HeroService)],
)
class HeroListComponent implements OnInit{

  final HeroService _heroService;
  HeroListComponent(this._heroService);
  List<Hero> heros;
  Hero selected;

  void onSelect(Hero hero) => selected = hero;
//  Sync method
//  void _getHeros() => _heroService.getAll().then((List<Hero> heros) => this.heros = heros);

  Future<void> _getHeros() async => heros = await _heroService.getAll();

  @override
  void ngOnInit() {
    _getHeros();
  }
}